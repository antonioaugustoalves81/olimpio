/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antonioalves.jvenda.beans;

import com.antonioalves.jvenda.java.Categoria;
import com.antonioalves.jvenda.java.Conexao;
import com.antonioalves.jvenda.utils.InterJvenda;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ANTONIO
 */
public class CategoriaBean implements InterJvenda{
    Categoria categoria = new Categoria();

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public CategoriaBean(Categoria categoria) {
        this.categoria = categoria;
    }
    
    
    

    @Override
    public boolean save() {
         //To change body of generated methods, choose Tools | Templates.
         String sql = "Insert into categoria(nome_categoria) values(?)";
         try{
             Connection con = Conexao.getConexao();
             PreparedStatement pst = con.prepareStatement(sql);
             pst.setString(1,categoria.getNome());
             pst.executeUpdate();
             return true;
         }catch(SQLException ex){
             ex.printStackTrace();
             return false;
         }
    }

    @Override
    public boolean findById() {
         //To change body of generated methods, choose Tools | Templates.
         String sql = "SELECT nome FROM categoria where id_categoria = "+categoria.getId();
         try{
             Connection con = Conexao.getConexao();
             PreparedStatement pst = con.prepareStatement(sql);
             ResultSet rs = pst.executeQuery();
             rs.next();
             if(rs.getRow() > 0){
                 categoria.setNome(rs.getString("nome_categoria"));
                 return true;
             }else{
                 System.out.println("A busca não retornou resultados");
                 return false;
             }
         }catch(SQLException ex){
             ex.printStackTrace();
             return false;
         }
    }

    @Override
    public boolean edit() {
        //To change body of generated methods, choose Tools | Templates.
        String sql = "UPDATE categoria SET nome_categoria = ? "
                +"WHERE id_categoria = ?";
        try{
            Connection con = Conexao.getConexao();
             PreparedStatement pst = con.prepareStatement(sql);
             pst.setString(1,categoria.getNome());  
             pst.setLong(2,categoria.getId());
             pst.executeUpdate();
             return true;
        }catch(SQLException ex){
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete() {
         //To change body of generated methods, choose Tools | Templates.
         String sql = "DELETE FROM categoria WHERE id_cateogria = ?";
         try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(sql);   
            pst.setLong(1,categoria.getId());
            pst.executeUpdate();
            return true;
         }catch(SQLException ex){
             ex.printStackTrace();
             return false;
         }
                 
    }
    
    public static void main(String[] args) {
        Categoria cat = new Categoria();
        cat.setNome("Livros");
        
        CategoriaBean bean = new CategoriaBean(cat);
        
        if(bean.save()){
            System.out.println("Success");
        }else{
            System.out.println("Error");
        }
    }
    
}
