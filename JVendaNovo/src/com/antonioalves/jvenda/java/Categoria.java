/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antonioalves.jvenda.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author ANTONIO
 */
public class Categoria {
    private Long id;
    private String nome;

    public Categoria() {
    }

    public Categoria(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public static void carregarCombo(JComboBox combo, Long cod){
        String query = "SELECT id_categoria, nome_categoria FROM categoria ORDER BY nome_categoria";
        
        try{
            Connection conexao = Conexao.getConexao();
            PreparedStatement pst = conexao.prepareStatement(query);
            ResultSet rs = pst.executeQuery(query);
            //Monta a combo box
            combo.removeAllItems();
            combo.addItem("SELECIONE UMA CATEGORIA DA LISTA");
            
            Categoria categoria = null;
            
            while(rs.next()){
                Categoria cat = new Categoria(rs.getLong("id_categoria"), rs.getString("nome_categoria"));
                
                if(cod == rs.getLong("id_categoria")){
                    categoria = cat;
                }
                combo.addItem(cat);
            }
            
            if(categoria != null){
                combo.setSelectedItem(categoria);
            }
            
            
            
        }catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,"Erro ao carregar as categorias.\n"
            +"Mensagem do servidor: "+e.getMessage());
        }
        
    }
}
