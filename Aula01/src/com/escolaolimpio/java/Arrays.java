/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.java;

/**
 *
 * @author ANTONIO
 */
public class Arrays {
    public static void main(String[] args) {
        String [] estados = {"RS", "SC", "PR", "SP"};
        
        //Usando forEach para iterar sobre arrays
        for(String uf:estados){
            System.out.println(uf);
        }
    }
    
}
