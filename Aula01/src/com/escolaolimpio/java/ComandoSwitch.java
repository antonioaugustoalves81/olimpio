/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.java;

/**
 *
 * @author ANTONIO
 */
public class ComandoSwitch {
    public static void main(String[] args) {
        int estacao = 4;
        
        switch(estacao){
            case 1:
                System.out.println("Versão");
                break;
                
            case 2:
                System.out.println("Outono");
                break;
            
            case 3:
                System.out.println("Inverno");
                break;
                
            case 4:
                System.err.println("Primavera");
                break;
                
            default:
                System.out.println("Erro de processamento");
                break;
        }
    }
}
