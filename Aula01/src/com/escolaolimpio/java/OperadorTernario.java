/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.java;

/**
 *
 * @author ANTONIO
 */
public class OperadorTernario {
    
    public static void main(String[] args) {
        String teste;
        int numero = 14;
        //opeerador ternário
        teste = numero%2 == 0?"O número é par":"O número é  impar";
        System.out.println("Resultado para "+numero+": "+teste);
        
        System.out.println("************************************************");
        //outro exemplo
        String tipo = "V";
        int estoque = 10;
        int itens = 3;
        int operacao = tipo.equals("C")?estoque+itens:estoque-itens;
        System.out.println("Estoque atual: "+operacao);
        
        
    }
    
}
