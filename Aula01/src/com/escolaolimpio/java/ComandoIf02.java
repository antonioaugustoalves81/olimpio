/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.java;

/**
 *
 * @author ANTONIO
 */
public class ComandoIf02 {
    public static void main(String[] args) {
        int a = 10;
        int b = 4;
        int c = 8;
        int total = 0;
        
        if(a > 2){
            if(b>3 && c>5){
                total = (a+b)*(c-b);
            }else{
                total = (a*b)+(c*b);
            }
        }else{
            total = (c-a)*(b*c*a);
        }
        System.out.println("Resultado: "+total);
    }
    
}
