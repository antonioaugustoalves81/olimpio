/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.java;

/**
 *
 * @author ANTONIO
 */
public class Variaveis {
    public static void main(String[] args) {
        int i = 10;
        double d = 23.45;
        String str = "Curso de Java";
        boolean b = true;
        char c = 'F';
        
        System.out.println("Inteiro: "+ i);
        System.out.println("Double: "+ d);
        System.out.println("String: "+ str);
        System.out.println("Boolean: "+ b);
        System.out.println("Caractere: "+c);
    }
    
}
