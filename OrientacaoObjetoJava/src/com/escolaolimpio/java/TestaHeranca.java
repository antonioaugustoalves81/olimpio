/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.java;

import com.escolaolimpio.classes.Categoria;
import com.escolaolimpio.classes.Livro;

/**
 *
 * @author ANTONIO
 */
public class TestaHeranca {
    public static void main(String[] args) {
        Livro livro = new Livro();
        
        livro.setCodigoProduto(1);
        livro.setTitulo("Teste");
        livro.setCategoria(new Categoria(3,"Livro"));
        livro.setAutor("Machado de Assis");
        livro.setPaginas(235);
        livro.setPreco(43.99);
        livro.setQuantidade(20);
        livro.setNome(livro.getTitulo());
        
        livro.mostraDetalhes();
    }
    
}
