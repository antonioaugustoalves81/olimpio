/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.java;

import com.escolaolimpio.classes.Categoria;
import com.escolaolimpio.classes.Produto;

/**
 *
 * @author ANTONIO
 */
public class OrientacaoObjetoJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Categoria cat = new Categoria();
        cat.setCodigo(2);
        cat.setNome("Televisores");
        Categoria cat2 = new Categoria(1,"Celulares");
        System.out.println("Cóodigo:"+cat.getCodigo());
        System.out.println("Categoria:"+cat.getNome());
        System.out.println("*************************************************");
        System.out.println("Cóodigo:"+cat2.getCodigo());
        System.out.println("Categoria:"+cat2.getNome());
        System.out.println("*************************************************");
        
        Produto prod = new Produto();
        prod.setCodigoProduto(1);
        prod.setNome("Motorola Moto G5s");
        prod.setPreco(899);
        prod.setQuantidade(30);
        prod.setCategoria(cat2);
        
        prod.mostrarDetalhes();
    }
    
}
