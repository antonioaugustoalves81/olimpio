/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.classes;

/**
 *
 * @author ANTONIO
 */
public class TrabalhandoComStrings {
    public static void main(String[] args) {
        String str = "ANTONIO AUGUSTO ALVES";
        int size = str.length();
        System.out.println("A string é: "+str);
        System.out.println("Numero de letras: "+str.length());
        System.out.println("Posição da letra L: "+str.indexOf("L"));
        System.out.println("10º caracteres: "+str.charAt(9));
        //informar posição Inicial e a posição final
        System.out.println("Nome do meio: "+str.substring(8,15));
    }
    
}
