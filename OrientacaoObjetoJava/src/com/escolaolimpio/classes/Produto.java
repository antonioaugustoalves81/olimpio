/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.classes;

/**
 *
 * @author ANTONIO
 */
public class Produto {
    private int codigoProduto;
    private String nome;
    private double preco;
    private double quantidade;
    private Categoria categoria;

    public int getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(int codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    public double getPrecoVenda(double lucro){
        return this.preco = this.preco+lucro;
    }
    
    
    
    public void mostrarDetalhes(){
        System.out.println("Código:"+this.getCodigoProduto());
        System.out.println("Produto:"+this.getNome());
        System.out.println("Estoque:"+this.getQuantidade());
        System.out.println("Preço:"+this.getPreco());
        System.out.println("Preço final:"+this.getPrecoVenda(34.50));
        System.out.println("Categoria:"+this.getCategoria().getNome());
    }
    
    
    
}
