/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.escolaolimpio.classes;

/**
 *
 * @author ANTONIO
 */
//Entendendo herança no Java
public class Livro extends Produto{
    private String titulo;
    private String autor;
    private int paginas;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }
    
    public void mostraDetalhes(){
        System.out.println("Código:"+this.getCodigoProduto());
        System.out.println("Produto:"+this.getNome());
        System.out.println("Estoque:"+this.getQuantidade());
        System.out.println("Preço:"+this.getPreco());
        System.out.println("Preço final:"+this.getPrecoVenda(34.50));
        System.out.println("Categoria:"+this.getCategoria().getNome());
        System.out.println("Autor:"+this.getAutor());
        System.out.println("Paginas:"+this.getPaginas());
        
    }
    
}
